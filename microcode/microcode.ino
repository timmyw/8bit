
#define SHIFT_DATA 2
#define SHIFT_CLK 3
#define SHIFT_LATCH 4
#define WRITE_EN 13

#define EEPROM_D0 5
#define EEPROM_D7 12

/* Which EEPROM are we programming? */
/*
#define EEPROM_LEFT 1
#define EEPROM_RIGHT 2
*/

/* Set up the signals
 */
#define FI    (1 << 15)
#define EO		(1 << 14)
#define SU		(1 << 13)
#define BI		(1 << 12)
#define OI		(1 << 11)
#define CE		(1 << 10)
#define CO		(1 << 9)
#define J	  	(1 << 8)
#define HLT	  (1 << 7)
#define MI		(1 << 6)
#define RI		(1 << 5)
#define RO		(1 << 4)
#define IO		(1 << 3)
#define II		(1 << 2)
#define AI		(1 << 1)
#define AO    1

/* The instruction codes.
	 Currently only LDA, ADD, OUT and HLT are defined
*/
#define I_NOP 0b0000
#define I_LDA 0b0001
#define I_ADD 0b0010
#define I_SUB 0b0011
#define I_STA 0b0100
#define I_LDI 0b0101
#define I_JMP 0b0110
#define I_7   0b0111
#define I_8   0b1000
#define I_9   0b1001
#define I_10  0b1010
#define I_11  0b1011
#define I_12  0b1100
#define I_13  0b1101
#define I_OUT 0b1110
#define I_HLT 0b1111

#define I_END 99

/* Print out a binary value */
#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0') 

/* Maps each instruction code to it's list of microcodes */
typedef struct MICRO {
	char instruction;
	short codes[5];
} micro;

/* The microcodes for each instruction
 */
micro codes[I_END+1] = {
  { I_NOP, {MI|CO, RO|II|CE, /**/ 0,0,0}},
  { I_LDA, {MI|CO, RO|II|CE, /**/ MI|IO, RO|AI, 0 }},
  { I_ADD, {MI|CO, RO|II|CE, /**/ MI|IO, RO|BI, AI|EO|FI }},
  { I_SUB, {MI|CO, RO|II|CE, /**/ MI|IO, RO|BI, AI|EO|SU|FI }},
  { I_STA, {MI|CO, RO|II|CE, /**/ MI|IO, AO|RI, 0 }},
  { I_LDI, {MI|CO, RO|II|CE, /**/ IO|AI, 0,     0 }},
  { I_JMP, {MI|CO, RO|II|CE, /**/ IO|J,  0,     0 }},
  { I_7,   {MI|CO, RO|II|CE, /**/ 0,     0,     0 }},
  { I_8,   {MI|CO, RO|II|CE, /**/ 0,     0,     0 }},
  { I_9,   {MI|CO, RO|II|CE, /**/ 0,     0,     0 }},
  { I_10,  {MI|CO, RO|II|CE, /**/ 0,     0,     0 }},
  { I_11,  {MI|CO, RO|II|CE, /**/ 0,     0,     0 }},
  { I_12,  {MI|CO, RO|II|CE, /**/ 0,     0,     0 }},
  { I_13,  {MI|CO, RO|II|CE, /**/ 0,     0,     0 }},
  { I_OUT, {MI|CO, RO|II|CE, /**/ AO|OI, 0,     0 }},
  { I_HLT, {MI|CO, RO|II|CE, /**/ HLT  , 0,     0 }},
  { I_END, {0,0,0,0,0}}
};

void writeMicrocodes() {
  char buf[129];
  for (unsigned char mc = 0; codes[mc].instruction != 99; mc++) {
    int address = codes[mc].instruction << 3;
    sprintf(buf, BYTE_TO_BINARY_PATTERN" %04x - ", BYTE_TO_BINARY(codes[mc].instruction), address);
    Serial.print(buf);

    char bufl[129]; memset(bufl, 0, sizeof(bufl));
    char bufr[129]; memset(bufr, 0, sizeof(bufr));
    for (int c = 0; c <= 7; c++) {
      short full = c < 5 ? codes[mc].codes[c] : 0;
      /* First byte */
      char b = (unsigned char)(full & 0xff);
      writeEEPROM(address, b);
      sprintf(buf, "%02x ", b);
      strcat(bufl, buf);

       /* Second byte */
      b = (unsigned char)((full >> 8) << 1);
      writeEEPROM(address + 128, b);
      sprintf(buf, "%02x ", b);
      strcat(bufr, buf);
      
      address++;
    }
    Serial.print(bufl);
    Serial.print(" ");
    Serial.println(bufr);
  }
}

void setAddress(int address, bool outputEnable) {
  shiftOut(SHIFT_DATA, SHIFT_CLK, MSBFIRST, (address >> 8) | (outputEnable ? 0x00 : 0x80));  
  shiftOut(SHIFT_DATA, SHIFT_CLK, MSBFIRST, address & 255);
  
  digitalWrite(SHIFT_LATCH, LOW);
  digitalWrite(SHIFT_LATCH, HIGH);
  digitalWrite(SHIFT_LATCH, LOW);
}

byte readEEPROM(int address) {
	setAddress(address, /*outputEnable*/ true);
	byte data = 0;
	for (int pin = EEPROM_D0; pin <= EEPROM_D7; pin++) {
		pinMode(pin, INPUT);
	}
	for (int pin = EEPROM_D7; pin >= EEPROM_D0; pin--) {
		data = (data << 1) + digitalRead(pin);
	}
	return data;
}

void displayEEPROM()
{
	for (int base = 0; base <= 255; base += 8) {
		byte data[16];
		for (int offset = 0; offset <= 7; offset++) {
			data[offset] = readEEPROM(base + offset);
		}

		char buffer[129];
		sprintf(buffer, BYTE_TO_BINARY_PATTERN" %03x : %02x %02x %02x %02x %02x %02x %02x %02x",
            BYTE_TO_BINARY(base),
						base,
						data[0], data[1], data[2], data[3],
						data[4], data[5], data[6], data[7]);
		Serial.println(buffer);
	}
}

void writeEEPROM(int address, byte data) {
	setAddress(address, false);

	for (int pin = EEPROM_D0; pin <= EEPROM_D7; pin++) {
		pinMode(pin, OUTPUT);
	}

	for (int pin = EEPROM_D0; pin <= EEPROM_D7; pin++) {
		digitalWrite(pin, data & 1);
		data = data >> 1;
	}
	digitalWrite(WRITE_EN, LOW);
	delayMicroseconds(1);
	digitalWrite(WRITE_EN, HIGH);
	delay(10);
}


void setup() {
  pinMode(SHIFT_DATA, OUTPUT);
  pinMode(SHIFT_CLK, OUTPUT);
  pinMode(SHIFT_LATCH, OUTPUT);

	digitalWrite(WRITE_EN, HIGH);
  pinMode(WRITE_EN, OUTPUT);

	Serial.begin(57600);

	/* Uncomment out the section below to erase the EEPROM before
		 programming */
  /*
	Serial.print("Erasing EEPROM");
  for (int address = 0; address <= 2047; address++) {
    writeEEPROM(address, 0xff);

    if (address % 64 == 0) {
      Serial.print(".");
    }
  }
  Serial.println(" done");
  */

  
  Serial.println("Writing data");
  writeMicrocodes();
  Serial.println("done");

  Serial.println("Reading EEPROM");
	displayEEPROM();
}

void loop() {
}
