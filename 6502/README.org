#+TITLE: 6502 Breadboard computer

Based on [[https://eater.net/8bit][Ben Eater's 6502]] project.

* Stage 1

  Creating a ROM binary image using python ([[./makerom.py][makerom.py]]), and a 6502
  monitor using an Arduino Mega ([[./6502-monitor][6502-monitor]]).

